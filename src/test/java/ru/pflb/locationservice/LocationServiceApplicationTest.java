package ru.pflb.locationservice;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ru.pflb.locationservice.entity.Location;
import ru.pflb.locationservice.entity.Type;
import ru.pflb.locationservice.service.LocationService;

@SpringBootTest
@AutoConfigureMockMvc
public class LocationServiceApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LocationService service;

    @Test
    public void shouldReturnLocationMessageFromService() throws Exception {
        Long id = 1L;
        Type type = new Type(1L,"service", null);
        Location location = new Location(1L,"Moscow", type);
        when(service.getLocation(id)).thenReturn(location);

        this.mockMvc
                .perform(get("/api/v1/location/" + id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"id\":1,\"address\":\"Moscow\",\"typeId\":1}")));
    }
}