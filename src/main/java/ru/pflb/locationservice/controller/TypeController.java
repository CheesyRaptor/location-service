package ru.pflb.locationservice.controller;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.pflb.locationservice.dto.TypeDto;
import ru.pflb.locationservice.entity.Location;
import ru.pflb.locationservice.entity.Type;
import ru.pflb.locationservice.service.TypeService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/type")
@AllArgsConstructor
public class TypeController {
    private final TypeService typeService;
    private final ModelMapper modelMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TypeDto createType(@RequestBody TypeDto typeDto) {
        return convertTypeToDto(typeService.createType(modelMapper.map(typeDto, Type.class)));
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TypeDto getType(@PathVariable Long id) {
        return convertTypeToDto(typeService.getType(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TypeDto updateType(@PathVariable Long id, @RequestBody TypeDto typeDto) {
        typeDto.setId(id);
        return convertTypeToDto(typeService.updateType(modelMapper.map(typeDto, Type.class)));
    }

    @GetMapping("/page")
    @ResponseStatus(HttpStatus.OK)
    public List<TypeDto> getTypes(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                  @RequestParam(value = "size", defaultValue = "10") Integer size,
                                  @RequestParam(value = "q", required = false, defaultValue = "id") String sort,
                                  @RequestParam(value = "order", required = false, defaultValue = "asc") String dir) {
        List<Type> types = typeService.getTypesList(page, size, dir, sort);

        return types.stream().map(this::convertTypeToDto).collect(Collectors.toList());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Boolean deleteType(@PathVariable Long id) {
        return typeService.deleteType(id);
    }

    private TypeDto convertTypeToDto(Type type) {
        TypeDto typeDto = modelMapper.map(type, TypeDto.class);
        if (type.getLocations() != null) {
            typeDto.setLocationIds(type.getLocations().stream().map(Location::getId).collect(Collectors.toList()));
        }

        return typeDto;
    }
}
