package ru.pflb.locationservice.controller;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.pflb.locationservice.dto.LocationDto;
import ru.pflb.locationservice.entity.Location;
import ru.pflb.locationservice.service.LocationService;
import ru.pflb.locationservice.service.TypeService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/location")
@AllArgsConstructor
public class LocationController {
    private final LocationService locationService;
    private final ModelMapper modelMapper;
    private final TypeService typeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public LocationDto createLocation(@RequestBody LocationDto locationDto) {
        return modelMapper.map(locationService.createLocation(toLocationEntity(locationDto)), LocationDto.class);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public LocationDto getLocation(@PathVariable Long id) {
        return modelMapper.map(locationService.getLocation(id), LocationDto.class);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public LocationDto updateLocation(@PathVariable Long id, @RequestBody LocationDto locationDto) {
        locationDto.setId(id);
        return modelMapper.map(locationService.updateLocation(modelMapper.map(locationDto, Location.class)), LocationDto.class);
    }

    @GetMapping("/page")
    @ResponseStatus(HttpStatus.OK)
    public List<LocationDto> getLocations(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                          @RequestParam(value = "size", defaultValue = "10") Integer size,
                                          @RequestParam(value = "q", required = false, defaultValue = "id") String sort,
                                          @RequestParam(value = "order", required = false, defaultValue = "asc") String dir) {
        List<Location> locations = locationService.getLocationsList(page, size, dir, sort);

        return locations.stream().map(l -> modelMapper.map(l, LocationDto.class)).collect(Collectors.toList());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Boolean deleteLocation(@PathVariable Long id) {
        return locationService.deleteLocation(id);
    }


    private Location toLocationEntity(LocationDto locationDto) {
        Location location = modelMapper.map(locationDto, Location.class);
        try {
            location.setType(typeService.getType(locationDto.getTypeId()));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return location;
    }
}
