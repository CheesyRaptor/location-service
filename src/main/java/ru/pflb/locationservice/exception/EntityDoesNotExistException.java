package ru.pflb.locationservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EntityDoesNotExistException extends IllegalStateException {
    public EntityDoesNotExistException() {
        super("Entity does no exist");
    }
}
