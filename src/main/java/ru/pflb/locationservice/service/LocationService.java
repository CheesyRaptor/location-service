package ru.pflb.locationservice.service;

import ru.pflb.locationservice.entity.Location;

import java.util.List;

public interface LocationService {
    Location getLocation(Long id);

    Location createLocation(Location location);

    Location updateLocation(Location location);

    List<Location> getLocationsList(int page, int size, String sortDir, String sort);

    boolean deleteLocation(Long id);
}
