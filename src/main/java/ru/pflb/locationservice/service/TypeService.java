package ru.pflb.locationservice.service;

import ru.pflb.locationservice.entity.Type;

import java.util.List;

public interface TypeService {
    Type getType(Long id);

    Type createType(Type type);

    Type updateType(Type type);

    boolean deleteType(Long id);

    List<Type> getTypesList(int page, int size, String sortDir, String sort);
}
