package ru.pflb.locationservice.service.implementation;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.pflb.locationservice.entity.Type;
import ru.pflb.locationservice.exception.EntityDoesNotExistException;
import ru.pflb.locationservice.repository.TypeRepository;
import ru.pflb.locationservice.service.TypeService;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class TypeServiceImpl implements TypeService {
    private final TypeRepository typeRepository;

    @Override
    public Type getType(Long id) {
        return typeRepository.getById(id).orElseThrow(IllegalStateException::new);
    }

    @Override
    public Type createType(Type type) {
        Long typeId = typeRepository.save(type).getId();
        Type createdType = typeRepository.getById(typeId).orElseThrow(IllegalStateException::new);

        log.info("New Type saved: " + createdType.toString());
        return createdType;
    }

    @Override
    public Type updateType(Type type) {
        if (type.getId() == null) {
            throw new IllegalStateException("No id specified for update");
        }

        if (!typeRepository.existsById(type.getId())) {
            throw new EntityDoesNotExistException();
        }

        if (type.getLocations() == null) {
            type.setLocations(typeRepository.getById(type.getId()).orElseThrow().getLocations());
        }

        typeRepository.save(type);

        Type updatedType = typeRepository.getById(type.getId()).orElseThrow();

        log.info("Type updated: " + updatedType.toString());
        return updatedType;
    }

    @Override
    public boolean deleteType(Long id) {
        typeRepository.deleteById(id);
        boolean deleted = typeRepository.findById(id).isEmpty();
        if (deleted) {
            log.info("Type deleted by ID: " + id.toString());
        }

        return deleted;
    }

    @Override
    public List<Type> getTypesList(int page, int size, String sortDir, String sort) {
        PageRequest pageReq = PageRequest.of(page, size, Sort.Direction.fromString(sortDir), sort);

        return typeRepository.findAll(pageReq).getContent();
    }
}
