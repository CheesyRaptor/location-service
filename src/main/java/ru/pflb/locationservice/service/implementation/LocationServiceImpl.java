package ru.pflb.locationservice.service.implementation;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.pflb.locationservice.entity.Location;
import ru.pflb.locationservice.exception.EntityDoesNotExistException;
import ru.pflb.locationservice.repository.LocationRepository;
import ru.pflb.locationservice.service.LocationService;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class LocationServiceImpl implements LocationService {
    private final LocationRepository locationRepository;

    @Override
    public Location getLocation(Long id) {
        return locationRepository.getById(id).orElseThrow(IllegalStateException::new);
    }

    @Override
    public Location createLocation(Location location) {

        Long locationId = locationRepository.save(location).getId();

        Location createdLocation = locationRepository
                .getById(locationId)
                .orElseThrow(IllegalStateException::new);

        log.info("New Location saved: " + createdLocation.toString());
        return createdLocation;
    }

    @Override
    public Location updateLocation(Location location) {
        if (location.getId() == null) {
            throw new IllegalStateException("No id specified for update");
        }

        if (!locationRepository.existsById(location.getId())) {
            throw new EntityDoesNotExistException();
        }

        locationRepository.save(location);

        Location updatedLocation = locationRepository.getById(location.getId()).orElseThrow();

        log.info("Location updated: " + updatedLocation.toString());
        return updatedLocation;
    }

    @Override
    public List<Location> getLocationsList(int page, int size, String sortDir, String sort) {
        PageRequest pageReq = PageRequest.of(page, size, Sort.Direction.fromString(sortDir), sort);

        return locationRepository.findAll(pageReq).getContent();
    }

    @Override
    public boolean deleteLocation(Long id) {
        locationRepository.deleteById(id);
        boolean deleted = locationRepository.findById(id).isEmpty();
        if (deleted) {
            log.info("Location deleted by ID: " + id.toString());
        }

        return deleted;
    }
}
